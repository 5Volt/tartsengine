#include "TestController.h"

#include <iostream>

#include <GLFW\glfw3.h>

#include <TartsEngine\InputEvent.h>

using namespace std;
using namespace GMath;

bool TestController::HandleInputEvent(InputEvent const & evt)
{
	cout << "Registered input event, Key:" << evt.key << " HID device: " << evt.inputController << endl;

	if (evt.key == GLFW_KEY_ESCAPE)
		state->stopped = true;
	else
		state->mainCamera.TranslateWorldSpace(vec3(0.1, 0, 0));

	return true;
}

#include "TestGameInstance.h"

#include <iostream>
#include <memory>

#include <TartsEngine\InputListener.h>

#include <GMath3d.h>

using namespace std;
using namespace GMath;

TestGameInstance::TestGameInstance()
	: GameInstance<TestGameState,TestController>()
	, package("./TestGameAssets/")
{
}


TestGameInstance::~TestGameInstance()
{
}

void TestGameInstance::FrameTick(double deltaT)
{
	//main draw loop

	main_gc->ClearBuffer(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	main_gc->SetShader(*myShader);
	
	state->mainCamera.RotateWorldSpace(vec3(0,0,1), 2 * GMath::PI * deltaT / 10);
	state->mainCamera.SetInShader(*myShader);
	
	mainMesh->DrawWithShader(*myShader);

	__super::FrameTick(deltaT);
}

bool TestGameInstance::Initialise()
{
	
	mainMesh = make_shared<StaticMesh>();

	myShader = GetAssetPackage().GetAsset<Shader>("mainShader");
	mainMesh = GetAssetPackage().GetAsset<StaticMesh>("mainMesh");
	mainMesh->Load();

	//get uniform handles
	mvp_location = myShader->GetVariable(ShaderUniform::MVP).location;

	return __super::Initialise();
}

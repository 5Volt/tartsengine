#pragma once
#include <TartsEngine\GameInstance.h>

#include "TestController.h"
#include "TestGameState.h"


#include <TartsEngine\Camera.h>
#include <TartsEngine\Shader.h>
#include <TartsEngine\StaticMesh.h>

#include <TartsEngine\Util\Asset.h>

class TestGameInstance :
	public GameInstance<TestGameState,TestController>
{
public:
	TestGameInstance();
	~TestGameInstance();

	// Inherited via InputHandler

	void FrameTick(double deltaT) override;
	bool Initialise() override;

	const AssetPackage &GetAssetPackage() {
		return package;
	}

private:
	AssetPackage package;

	GLint mvp_location;
	std::shared_ptr<Shader> myShader;

	GMath::mat4 mvp;

	std::shared_ptr<StaticMesh> mainMesh;

};


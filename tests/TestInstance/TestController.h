#pragma once

#include<memory>

#include <TartsEngine\GameController.h>

#include "TestGameState.h"

class TestController :
	public GameController<TestGameState>
{
public:
	TestController(std::shared_ptr<TestGameState> &_state) : GameController<TestGameState>(_state) {};
	~TestController() = default;

	virtual bool HandleInputEvent(InputEvent const & evt) override;
};


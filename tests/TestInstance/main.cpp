
#include <iostream>
#include <string>

#include "TestGameInstance.h"
#include <TartsEngine\Shader.h>
#include <TartsEngine\Interfaces\Serialisable.h>

using namespace std;


void main()
{
	TestGameInstance game;

	game.Run();
	
}
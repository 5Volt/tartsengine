// EngineTest.cpp : Defines the entry point for the testing application.

#include <iostream>

#include "gtest\gtest.h"

int main(int ac, char **av)
{
	testing::InitGoogleTest(&ac, av);
	int rc = RUN_ALL_TESTS();
	std::cin.get();
	return rc;
}


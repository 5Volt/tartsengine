#pragma once

#include <thread>
#include <future>

#include <TartsEngine\Tarts.h>

#include "gtest\gtest.h"

#include "GLFW\glfw3.h"

#include "TartsEngine/TartsEngine.h"
#include "TartsEngine/GraphicsContext.h"
#include "TartsEngine/Shader.h"

#include "TestHarness.h"


static const char* vertex_shader_text =
"#version 110\n"
"uniform mat4 MVP;\n"
"attribute vec3 vCol;\n"
"attribute vec2 vPos;\n"
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
"    color = vCol;\n"
"}\n";

static const char* fragment_shader_text =
"#version 110\n"
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_FragColor = vec4(color, 1.0);\n"
"}\n";

TEST(GraphicsTests, Shader_compile)
{

	//Initialise test objects
	EXPECT_EQ(true, TartsEngine::Initialise()) << "engine Initialisation failed";

	///create the shader
	ShaderSource source;

	source.vertexSource = vertex_shader_text;
	source.fragmentSource = fragment_shader_text;

	
	EXPECT_NO_THROW(Shader myShader(std::move(source))) << "Shader failed to compile";

}

TEST(GraphicsTests, Shader_varFind)
{

	//Initialise test objects
	EXPECT_EQ(true, TartsEngine::Initialise()) << "engine Initialisation failed";

	///create the shader
	ShaderSource source;

	source.vertexSource = vertex_shader_text;
	source.fragmentSource = fragment_shader_text;

	source.variables.emplace_back(ShaderUniform::MVP, "MVP");
	source.variables.emplace_back(ShaderAttribute::VERTPOS, "vPos");
	source.variables.emplace_back(ShaderAttribute::VERTCOL, "vCol");

	EXPECT_NO_THROW(Shader myShader(std::move(source))) << "Shader failed to compile";
	
	Shader myShader(std::move(source));

	EXPECT_NE(myShader.GetVariable(ShaderUniform::MVP).location, -1);
	EXPECT_NE(myShader.GetVariable(ShaderAttribute::VERTPOS).location,-1);
	EXPECT_NE(myShader.GetVariable(ShaderAttribute::VERTCOL).location,-1);

	EXPECT_EQ(myShader.GetVariable(ShaderAttribute::VERTNORM).location, -1);

	EXPECT_NE(myShader.GetVariable("MVP").location, -1);
	EXPECT_NE(myShader.GetVariable("vPos").location, -1);
	EXPECT_NE(myShader.GetVariable("vCol").location, -1);

	EXPECT_EQ(myShader.GetVariable("nonReal").location, -1);

}

TEST(GraphicsTests, Shader_Serialise)
{
	TartsEngine::Initialise();

	///create the shader
	ShaderSource source;

	source.vertexSource = vertex_shader_text;
	source.fragmentSource = fragment_shader_text;

	source.variables.emplace_back(ShaderUniform::MVP, "MVP");
	source.variables.emplace_back(ShaderAttribute::VERTPOS, "vPos");
	source.variables.emplace_back(ShaderAttribute::VERTCOL, "vCol");

	Shader myShader(std::move(source));
	std::stringstream sstream;
	sstream << myShader;

	//it's too complex to write a full expectation for the serialisation
	// so we just test it by deserialising a shader from the serialisation
	//and check that it compiles correctly
	Shader fromStreamShader = Serialisable::CreateFromStream<Shader>(sstream);

	EXPECT_NE(fromStreamShader.GetHandle(), -1);
	EXPECT_NE(fromStreamShader.GetVariable(ShaderUniform::MVP).location, -1);
	EXPECT_NE(fromStreamShader.GetVariable(ShaderAttribute::VERTPOS).location, -1);
	EXPECT_NE(fromStreamShader.GetVariable(ShaderAttribute::VERTCOL).location, -1);

	EXPECT_EQ(fromStreamShader.GetVariable(ShaderAttribute::VERTNORM).location, -1);

}
#pragma once

#include <thread>
#include <future>

#include <TartsEngine\Tarts.h>

#include "gtest\gtest.h"

#include "GLFW\glfw3.h"

#include "TartsEngine/TartsEngine.h"
#include "TartsEngine/GraphicsContext.h"

#include "TestHarness.h"

TEST(BasicTests, Engine_Initialisation)
{
	//Initialise test objects
	EXPECT_EQ(true, TartsEngine::Initialise())<< "engine Initialisation failed";

	EXPECT_EQ(true, TartsEngine::isInitialised()) << "engine check initialisation failed";

}


TEST(BasicTests, GraphicsContext_Initialisation)
{
	//Initialise test objects
	GraphicsContext gc;
	
	EXPECT_EQ(glfwGetCurrentContext(), gc.GetWindow());
}

//entry point for test second thread
template<bool init_return, bool stopself>
void runGame(std::promise<int> &&p)
{
	TestGameInstance<init_return, stopself> instance;
	p.set_value(0);

	try {
		instance.Run();
	}
	catch (...) {
		p.set_value(1);
	}

	p.set_value(2);

}

template<int millis>
void stopAfter(std::promise<bool> &&stop)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(millis));
	stop.set_value(true);
}

TEST(BasicTests, GameInstance_Initialisation)
{
	std::promise<int> promise;
	auto future = promise.get_future();
	//TODO
	TestGameInstance<true> instance;
	std::promise<bool> stopPromise;
	instance.stopFuture = stopPromise.get_future();
	stopPromise.set_value(true);

	EXPECT_NO_THROW(instance.Run());

	EXPECT_EQ(instance.tickCount, 1);

}


TEST(BasicTests, DISABLED_GameInstance_FrameTick)
{
	//TODO
}


#include "gtest\gtest.h"

#include <TartsEngine\Tarts.h>
#include <TartsEngine\Interfaces\Serialisable.h>

#include "Mock/TestSerialTypes.h"

/************************/
/*   serialise tests    */
/************************/

TEST(SerialTests, Serialise_BuiltIn)
{

	ManyBuiltIn testVar{ 1,true,'c',1.5f,2.5 };

	std::stringstream sstream1;
	sstream1 << testVar;

	EXPECT_STREQ(sstream1.str().c_str(), "{1:1:c:1.5:2.5}");
	
	testVar.x = 5;
	testVar.b = false;
	testVar.c = 'b';
	testVar.f = 5;
	testVar.d = 50.42;

	std::stringstream sstream2;
	sstream2 << testVar;
	EXPECT_STREQ(sstream2.str().c_str(), "{5:0:b:5:50.42}");

}


TEST(SerialTests, Serialise_vectors)
{
	
	VectCont testVar{ {1,2,3}, {1.5,2.5,3.5} };

	std::stringstream sstream1;
	sstream1 << testVar;

	EXPECT_STREQ(sstream1.str().c_str(), "{[1,2,3,]:[1.5,2.5,3.5,]}");

	testVar.x.pop_back();
	testVar.y.push_back(4.5);

	std::stringstream sstream2;
	sstream2 << testVar;

	EXPECT_STREQ(sstream2.str().c_str(), "{[1,2,]:[1.5,2.5,3.5,4.5,]}");

}



TEST(SerialTests, Serialise_strings)
{

	StringCont testVar{ "testString1" };

	std::stringstream sstream1;
	sstream1 << testVar;

	EXPECT_STREQ(sstream1.str().c_str(), "{11|testString1}");
	
	testVar.str = "test String 1";

	std::stringstream sstream2;
	sstream2 << testVar;
	EXPECT_STREQ(sstream2.str().c_str(), "{13|test String 1}");

}



TEST(SerialTests, Serialise_Arrays)
{
	
	ArrayCont testVar({'a','b','c','d','e'});

	std::stringstream sstream1;
	sstream1 << testVar;

	EXPECT_STREQ(sstream1.str().c_str(), "{5|[a,b,c,d,e,]}");

}

TEST(SerialTests, Serialise_Enums)
{
	EnumCont testVar({ EnumCont::TestEnumClass::EVal1,EnumCont::EVal2 });

	std::stringstream sstream1;
	sstream1 << testVar;

	EXPECT_STREQ(sstream1.str().c_str(), "{1:2}");

}


/************************/
/*   Deserialise tests  */
/************************/
TEST(SerialTests, DeSerialise_BuiltIn)
{

	ManyBuiltIn testVar;

	std::stringstream sstream1("{1:1:c:1.5:2.5}");
	sstream1 >> testVar;

	EXPECT_EQ(testVar, (ManyBuiltIn{ 1,true,'c',1.5f,2.5 }));


}


TEST(SerialTests, DeSerialise_vectors)
{

	
	VectCont testVar;
	std::stringstream sstream1("{[1,2,3,]:[1.5,2.5,3.5,]}");
	sstream1  >> testVar;

	EXPECT_EQ(testVar,(VectCont{ { 1,2,3 },{ 1.5,2.5,3.5 } }));

}



TEST(SerialTests, DeSerialise_strings)
{
	StringCont testVar;
	StringCont expectVar{ "testString1" };

	std::stringstream sstream1("{11|testString1}");
	sstream1 >> testVar;

	EXPECT_EQ(testVar, expectVar);

	expectVar.str = "test String 1";
	

	std::stringstream sstream2("{13|test String 1}");
	sstream2 >> testVar;
	
	EXPECT_EQ(testVar, expectVar);
	
	expectVar.str = "test {String} [1]";
	

	std::stringstream sstream3("{17|test {String} [1]}");
	sstream3 >> testVar;
	
	EXPECT_EQ(testVar, expectVar);

}

TEST(SerialTests, DeSerialise_Arrays)
{

	ArrayCont expectVar({ 'a','b','c','d','e' });
	ArrayCont testVar;

	std::stringstream sstream1( "{5|[a,b,c,d,e,]}");
	sstream1 >> testVar;

	EXPECT_EQ(testVar,expectVar);

}

TEST(SerialTests, DeSerialise_Enum)
{

	EnumCont testVar;

	std::stringstream sstream1("{2:1}");
	sstream1 >> testVar;

	EXPECT_EQ(testVar, (EnumCont{ EnumCont::TestEnumClass::EVal2,EnumCont::EVal1}));


}

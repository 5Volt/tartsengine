#pragma once

#include <TartsEngine\Interfaces\Serialisable.h>
#include <TartsEngine\Object.h>
#include <TartsEngine\Util\Asset.h>

/************************/
/*   Testing classes    */
/************************/
struct ManyBuiltIn : public Asset<ManyBuiltIn>
{
	int x;
	bool b;
	char c;
	float f;
	double d;


	bool operator==(const ManyBuiltIn & o) const
	{
		return x == o.x && b == o.b && c == o.c && f == o.f && d == o.d;
	}

	ManyBuiltIn() = default;
	ManyBuiltIn(int _x, bool _b, char _c, float _f, double _d)
		:x(_x), b(_b), c(_c), f(_f), d(_d) {}
	~ManyBuiltIn() = default;

	DECL_SERIAL(x, b, c, f, d);
};

struct VectCont : public Serialisable, public Object
{
	std::vector<int> x;
	std::vector<float> y;

	VectCont() = default;
	VectCont(std::vector<int> const &_x, std::vector<float> _y)
		: x(_x), y(_y) {};

	bool operator==(const VectCont & o) const
	{
		return x == o.x && y == o.y;
	}

	DECL_SERIAL(x, y)
};

struct StringCont : public Serialisable, public Object
{
	std::string str;

	StringCont() = default;

	StringCont(std::string const &_str)
		: str(_str) {};

	bool operator==(const StringCont & o) const
	{
		return str == o.str;
	}

	DECL_SERIAL(str);
};

struct ArrayCont : public Serialisable, public Object
{
	char constArray[5];

	ArrayCont() = default;
	ArrayCont(std::initializer_list<char> val) {
		if (val.size() != 5)
			throw 0;

		int x = 0;
		for (auto & val1 : val)
			constArray[x++] = val1;

	}

	bool operator==(const ArrayCont & o) const
	{
		bool eq = true;
		for (int x = 0; x < 5; ++x)
			eq &= (constArray[x] == o.constArray[x]);
		return eq;
	}

	DECL_SERIAL(serial_const_array(constArray, 5));


};


struct EnumCont : public Serialisable
{
	enum class TestEnumClass
	{
		EVal0,
		EVal1,
		EVal2
	};

	enum TestEnum
	{
		EVal0,
		EVal1,
		EVal2
	};

	EnumCont() = default;

	EnumCont(TestEnumClass _enumClass, TestEnum _rawEnum)
		:enumClass(_enumClass),rawEnum(_rawEnum)
	{}

	bool operator ==(EnumCont const &other)const
	{
		return other.enumClass == enumClass && other.rawEnum == other.rawEnum;
	}

	TestEnumClass enumClass;
	TestEnum rawEnum;

	DECL_SERIAL(enumClass, rawEnum);

};
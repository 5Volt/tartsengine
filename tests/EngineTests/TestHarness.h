#pragma once

#include <TartsEngine\GameInstance.h>

class TestGameController : public GameController<GameState>
{
public:
	TestGameController(std::shared_ptr<GameState> &_state) : GameController(_state) {}
	// Inherited via GameController
	virtual bool HandleInputEvent(InputEvent const & evt) override { return true; };
};

template<bool init_return = true>
class TestGameInstance : public GameInstance<GameState, TestGameController>
{
public:
	bool Initialise() override { return init_return; }

	void FrameTick(double deltatT) override { ++tickCount; state->stopped = stopFuture.valid(); }

	bool initialised = false;
	int tickCount = 0;

	std::future<bool> stopFuture;
};

#include "gtest\gtest.h"

#include <memory>

#include <TartsEngine\Tarts.h>
#include "TartsEngine/TartsEngine.h"
#include "TartsEngine/Shader.h"
#include <TartsEngine\Object.h>
#include <TartsEngine\Util\Asset.h>

#include "Mock\TestSerialTypes.h"


static AssetPackage package("./");

TEST(AssetTests, Assetise_BuiltIn_read)
{
	using std::fstream;

	auto strname = typeid(ManyBuiltIn).name();

	auto assetHeader = "{18|struct ManyBuiltIn}";
	auto assetSerialisation = "{1:1:c:1.5:2.5}";

	AssetFactory::RegisterAll<TypeList<ManyBuiltIn>>();

	std::stringstream readStream(assetSerialisation);

	ManyBuiltIn testVar;
	readStream >> testVar;

	fstream file("./builtIn" + ASSET_FILE_EXTENSION, fstream::out);
	EXPECT_TRUE(file.is_open()) << "unable to open file for writing";

	file << assetHeader << assetSerialisation;
	file.close();

	auto result = package.GetAsset<ManyBuiltIn>("./builtIn");

	EXPECT_EQ(testVar, *result);

	std::filesystem::remove("./builtIn" + ASSET_FILE_EXTENSION);

}

template<typename T>
struct TestIterator
{
	static bool Eval()
	{
		bool ret = AssetFactory::HasID(Asset<T>::id);
		ret &= AssetFactory::HasType<T>();

		auto blankType = AssetFactory::Create(T::GetStaticId());

		AssetFactory::FactoryPtrType<T> asType;
		asType = std::dynamic_pointer_cast<T>(blankType);

		ret &= bool(blankType);
		ret &= bool(asType);
		return ret;
	}
};

TEST(AssetTests, EngineAssets_RegisteredOnInit)
{

	using KnownEngineTypes = TypeList<Shader>;

	TartsEngine::Initialise();
	EXPECT_TRUE(AssetFactory::HasID(Asset<Shader>::id));
	EXPECT_TRUE(AssetFactory::HasType<Shader>());

	auto blankShader = AssetFactory::Create(Shader::GetStaticId());

	EXPECT_TRUE(blankShader);

	EXPECT_TRUE(KnownEngineTypes::ForAll < TestIterator > ());

}


TEST(AssetTests, Assetise_builtin_write)
{
	auto testVar = std::make_shared<ManyBuiltIn>(ManyBuiltIn{ 1,true,'c',1.5f,2.5 });

	std::filesystem::remove(package.GetAssetFilePath("testWrite"));
	package.WriteAsset(testVar,"testWrite");

	auto result = package.GetAsset<ManyBuiltIn>("testWrite");

	EXPECT_EQ(*testVar, *result);

	//clean up after test
	std::filesystem::remove(package.GetAssetFilePath("testWrite"));

}

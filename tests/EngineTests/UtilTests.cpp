
#include "gtest\gtest.h"

#include <TartsEngine\Tarts.h>
#include <TartsEngine\Util\Variant.h>


using Boolint = Variant<bool, int>;


TEST(UtilTests, Variant_Initialisation)
{
	Boolint testVar;
	
	EXPECT_EQ(testVar.active, VariantImplementation::VAR_ID_NULL);
	EXPECT_ANY_THROW(testVar.get<bool>());
	EXPECT_ANY_THROW(testVar.get<int>());

}


TEST(UtilTests, Variant_Set)
{
	Boolint testVar;
	
	EXPECT_EQ(testVar.active, VariantImplementation::VAR_ID_NULL);

	testVar.set(true);
	EXPECT_EQ(testVar.active, decltype(testVar)::GetId<bool>::ID);
	EXPECT_NE(testVar.active, decltype(testVar)::GetId<int>::ID);
	
	bool val1;
	EXPECT_NO_THROW(val1 = testVar.get<bool>());
	EXPECT_EQ(val1,true);
	testVar.set(false);
	EXPECT_NO_THROW(val1 = testVar.get<bool>());
	EXPECT_EQ(val1, false);

	testVar.set(10);
	EXPECT_NE(testVar.active, decltype(testVar)::GetId<bool>::ID);
	EXPECT_EQ(testVar.active, decltype(testVar)::GetId<int>::ID);

	int val2;
	EXPECT_NO_THROW(val2 = testVar.get<int>());
	EXPECT_EQ(val2, 10);

}

TEST(UtilTests, Variant_Get)
{
	Boolint testVar;

	EXPECT_EQ(testVar.active, VariantImplementation::VAR_ID_NULL);

	testVar.set(true);

	bool val1;
	int val2;

	EXPECT_NO_THROW(val1 = testVar.get<bool>());
	EXPECT_ANY_THROW(val2 = testVar.get<int>());
	EXPECT_EQ(val1, true);
	testVar.set(false);
	EXPECT_NO_THROW(val1 = testVar.get<bool>());
	EXPECT_ANY_THROW(val2 = testVar.get<int>());
	EXPECT_EQ(val1, false);

	testVar.set(10);

	EXPECT_ANY_THROW(val1 = testVar.get<bool>());
	EXPECT_NO_THROW(val2 = testVar.get<int>());
	EXPECT_EQ(val2, 10);
}

TEST(UtilTests, Variant_noset_invalid)
{
	using VariantImplementation::VAR_ID_NULL;
	
	EXPECT_EQ(Boolint::GetId<double>::ID, VAR_ID_NULL);
	EXPECT_EQ(Boolint::GetId<char>::ID, VAR_ID_NULL);
	EXPECT_EQ(Boolint::GetId<void>::ID, VAR_ID_NULL);
	EXPECT_EQ(Boolint::GetId<bool *>::ID, VAR_ID_NULL);
	EXPECT_EQ(Boolint::GetId<bool const*>::ID, VAR_ID_NULL);
	EXPECT_EQ(Boolint::GetId<bool const>::ID, VAR_ID_NULL);

	EXPECT_NE(Boolint::GetId<bool>::ID, VAR_ID_NULL);
	EXPECT_NE(Boolint::GetId<int>::ID, VAR_ID_NULL);
}

TEST(UtilTests, Variant_Serialise)
{
	Boolint testVar;

	std::stringstream sstream;
	// should print as character string 'DCBA'
	// (order is reversed due to endianness)
	testVar.set(0x41424344);
	sstream << testVar;
	EXPECT_STREQ(sstream.str().c_str(), "{1:4|[D,C,B,A,]}");

}

TEST(UtilTests, Variant_Deserialise)
{
	Boolint testVar;

	std::istringstream sstream("{1:4|[D,C,B,A,]}");
	sstream >> testVar;

	EXPECT_EQ(testVar.active, Boolint::GetId<int>::ID);
	EXPECT_EQ(testVar.get<int>(), 0x41424344);
	
}

TEST(UtilTests, Variant_Assignment)
{
	Boolint testVar, testVar2;
	
	testVar = true;

	EXPECT_EQ(testVar.active, Boolint::GetId<bool>::ID);
	EXPECT_EQ(testVar.get<bool>(), true);


	testVar = 25;

	EXPECT_EQ(testVar.active, Boolint::GetId<int>::ID);
	EXPECT_EQ(testVar.get<int>(), 25);


	testVar2 = testVar;

	EXPECT_EQ(testVar.active, Boolint::GetId<int>::ID);
	EXPECT_EQ(testVar.get<int>(), 25);
	
}



TEST(UtilTests, Variant_Comparison)
{
	Boolint testVar, testVar2;

	using FloatInt = Variant<float, int>;

	testVar = true;
	testVar2 = true;

	EXPECT_EQ(testVar, testVar2);

	testVar = 25;
	
	EXPECT_NE(testVar, testVar2);

	testVar2 = testVar;

	EXPECT_EQ(testVar, testVar2);

	EXPECT_EQ(testVar, 25);
	EXPECT_EQ(testVar, 25.0);

}

TEST(UtilTests, Variant_Copystruct)
{
	Boolint testVar;


	testVar = true;
	Boolint testVar2(testVar);

	EXPECT_EQ(testVar, testVar2);

	testVar = 3;
	Boolint testVar3(testVar);

	EXPECT_EQ(testVar, testVar3);
	EXPECT_EQ(testVar3, 3);

	Boolint testVar4(3);
	EXPECT_EQ(testVar4, testVar3);

	Boolint testVar5(true);
	EXPECT_EQ(testVar5, testVar2);
	EXPECT_EQ(testVar5, true);

}
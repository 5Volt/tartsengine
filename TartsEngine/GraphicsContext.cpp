#include "GraphicsContext.h"

#include <exception>
#include <memory>

#include <GMath.h>

#include "TartsEngine.h"
#include "Shader.h"

using namespace std;
using namespace GMath;

GraphicsContext::GraphicsContext()
{

	int width = 800, height = 600;

	if (!TartsEngine::Initialise())
		throw exception("Unable to initialise engine");
	
	main_window = glfwCreateWindow(width, height, "Game Window", NULL, NULL);// TODO get from setting
	
	glfwMakeContextCurrent(main_window);

	glViewport(0, 0, width, height);

}

GraphicsContext::~GraphicsContext()
{
}

void GraphicsContext::SwapBuffers()
{
	glfwSwapBuffers(main_window);
	glfwPollEvents();
}

void GraphicsContext::ClearBuffer(GLint bufferBits)
{
	glClear(bufferBits);
}

void GraphicsContext::SetShader(Shader const &shader)
{
	glUseProgram(shader.GetHandle());
}
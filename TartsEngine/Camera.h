#pragma once

#include "Tarts.h"

#include <GMath.h>

class Shader;

class Camera
{
public:
	TARTS_API
		Camera();
	TARTS_API
		~Camera();

	TARTS_API
		void RotateWorldSpace(GMath::vec3 axis, float radians);
	TARTS_API
		void TranslateWorldSpace(GMath::vec3 translation);

	TARTS_API
		void RotateLocal(GMath::vec3 axis, float radians);
	TARTS_API
		void TranslateLocal(GMath::vec3 translation);

	TARTS_API
		bool SetInShader(Shader &shader) const;

private:
	GMath::mat4 vpMatrix;

};


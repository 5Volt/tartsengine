#include "Shader.h"

#include <algorithm>

using namespace std;

Shader::Shader(ShaderSource &&_source)
	:source(_source)
{
	compile();
	FindVariables();
}


void compileShaderUnit(GLuint shaderID, string const & source)
{
	char const * sourcePtr = source.c_str();

	glShaderSource(shaderID, 1, &sourcePtr, NULL);
	glCompileShader(shaderID);

	//check for errors
	GLint Result = GL_FALSE;
	GLsizei InfoLogLength;

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength != 0)
	{
		string ShaderErrorMessage;
		ShaderErrorMessage.resize(InfoLogLength);
		glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &ShaderErrorMessage[0]);
		if (strlen(&ShaderErrorMessage[0]))
			throw exception(&ShaderErrorMessage[0]);
	}

}

void Shader::compile()
{
	//create and compile shaders
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

	compileShaderUnit(vs, source.vertexSource);
	compileShaderUnit(fs, source.fragmentSource);

	//link shader program
	GLuint fullShader = glCreateProgram();
	glAttachShader(fullShader, fs);
	glAttachShader(fullShader, vs);
	glLinkProgram(fullShader);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR) {
		string errorString = "GL error: ";
		errorString.append(string(reinterpret_cast<char const*>(glewGetErrorString(error))));
		throw std::exception(errorString.c_str());
	}

	shaderHandle = fullShader;

}

void Shader::FindVariables()
{

	for (auto &shadVar : source.variables)
	{
		if (shadVar.isAttribute())
			shadVar.location = glGetAttribLocation(shaderHandle, shadVar.name.c_str());
		else if (shadVar.isUniform())
			shadVar.location = glGetUniformLocation(shaderHandle, shadVar.name.c_str());
		else
			throw exception("Unknown variable type in shader");
	}
}

static const ShaderVariable dummyVar;

ShaderVariable const &Shader::GetVariable(ShaderVariable::VariableID id) const
{

	auto it = find_if(source.variables.begin(), source.variables.end(),
		[&id]
	(ShaderVariable const &var)
	{
		return var.ID == id;
	});

	if (it == source.variables.end())
		return dummyVar;

	return *it;
}

ShaderVariable const &Shader::GetVariable(std::string const &varName) const
{

	auto it = find_if(source.variables.begin(), source.variables.end(),
		[&varName]
	(ShaderVariable const &var)
	{
		return var.name == varName;
	});

	if (it == source.variables.end())
		return dummyVar;

	return *it;
}

void Shader::PostRead()
{
	compile();
	FindVariables();
}

#include "Camera.h"

#include "Shader.h"

#include <GMath3d.h>

using namespace std;
using namespace GMath;

Camera::Camera()
{
}


Camera::~Camera()
{
}

void Camera::RotateWorldSpace(vec3 axis, float radians)
{
	rotate(vpMatrix, axis, radians);
}

void Camera::TranslateWorldSpace(vec3 translation)
{
	translate(vpMatrix, translation);
}

void Camera::RotateLocal(vec3 axis, float radians)
{
	//TODO
}

void Camera::TranslateLocal(vec3 translation)
{
	//TODO
}

bool Camera::SetInShader(Shader &shader) const
{
	auto mvp_location = shader.GetVariable(ShaderUniform::MVP).location;
	
	if (mvp_location == -1)
		return false;

	glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*)vpMatrix.toArray());

	return true;
}

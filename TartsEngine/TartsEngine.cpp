#include "TartsEngine.h"

#include <exception>
#include <iostream>

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include "TartsEngineAssets.h"

using namespace std;

bool TartsEngine::is_initialised;

/**
* called by the engine in the event of a GLFW error
*/
void error_callback(int error, const char* description)
{
	cerr << description << endl;
}

bool TartsEngine::Initialise()
{
	if (is_initialised)
		return true;

	if (!glfwInit())
		throw exception("unable to initialise GLFW");

	GLFWwindow *win = glfwCreateWindow(1, 1, "window", NULL, NULL);
	glfwMakeContextCurrent(win);

	if (glewInit() != GLEW_OK)
		throw exception("unable to initialise GLEW");

	glfwSetErrorCallback(error_callback);

	glfwDestroyWindow(win);

	RegisterAllEngineAssets();
	
	is_initialised = true;

	return is_initialised;
}

bool TartsEngine::isInitialised()
{
	return is_initialised;
}
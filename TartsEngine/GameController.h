#pragma once

#include <memory>

#include "Tarts.h"

#include "Interfaces\InputHandler.h"

#include "GameState.h"

/**
 * Controller class for a game
 * This class shares ownership of a game state
 * It will handle incoming inputs including routing them to the relevant in-game classes
 * It will also handle high-level logic for controlling the game state
 *
 * @param StateType typename for the game state
 *
 * @see GameInstance
 **/
template<typename StateType>
class GameController : public InputHandler
{
	static_assert(std::is_base_of<GameState, StateType>::value,
		"GameInstance::StateType template param must be base class of GameState");

public:
	GameController(std::shared_ptr<StateType> &_state):state(_state) {}
	
	~GameController() {};

	std::shared_ptr<StateType> state;
};


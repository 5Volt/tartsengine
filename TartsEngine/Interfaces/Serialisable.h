#pragma once

#include <iostream>
#include <vector>
#include <string>

#include "../Tarts.h"
namespace SerialImplementation{
	static constexpr char SEP = ':';
	static constexpr char SIZE_SEP = '|';
	static constexpr char OBJ_START = '{';
	static constexpr char OBJ_END = '}';
	static constexpr char LIST_START = '[';
	static constexpr char LIST_END = ']';
	static constexpr char LIST_SEP = ',';

	/**********************/
	/*   helper types     */
	/**********************/
	template<typename T>
	struct remove_ptr_const {
		using type = std::add_pointer<std::remove_const<std::remove_pointer<const char*>::type>::type>::type;
	};

	template <typename T, typename Size_Type >
	struct Serial_array_impl
	{
		Serial_array_impl(T _loc, Size_Type const &_size)
			:loc(_loc), size(_size)
		{}

		static_assert(std::is_pointer<T>::value && std::is_integral<Size_Type>::value,
			"Serial Array must be constructed from pointer and integral type");

		T loc;
		Size_Type size;
	};


	/****************************/
	/*     Serial Tag Utility   */
	/****************************/

	template< typename... Rest>
	struct SerialTag_Impl;

	template<typename T, typename... Rest>
	struct SerialTag_Impl<T,  Rest...> {
		static_assert(!std::is_pointer<T>::value,
			"Serialised Array must provide size in following param");
		static void toStream(std::ostream &stream, T const &args, Rest const &... rest)
		{
			SerialTag_Impl<T>::toStream(stream, args);
			stream << SEP;
			SerialTag_Impl<Rest...>::toStream(stream, rest...);
		}
	
		static void fromStream(std::istream &stream, T &args, Rest & ... rest)
		{
			SerialTag_Impl<T>::fromStream(stream, args);
			Check(DSERIALISE, stream.get() == SEP);
			SerialTag_Impl<Rest...>::fromStream(stream, rest...);
		}
	};

	template<typename T, std::enable_if_t<!std::is_enum<T>::value,int> = 0>
	void globalToStream(std::ostream &stream, T const &args)
	{
		stream << args;
	}
	
	template<typename T, std::enable_if_t<!std::is_enum<T>::value, int> = 0>
	void globalFromStream(std::istream &stream, T &args)
	{
		stream >> args;
	}

	template<typename T, std::enable_if_t<std::is_enum<T>::value, int> = 0>
	void globalToStream(std::ostream &stream, T const &args)
	{
		stream << static_cast<int>(args);
	}

	template<typename T, std::enable_if_t<std::is_enum<T>::value, int> = 0>
	void globalFromStream(std::istream &stream, T &args)
	{
		int val;
		stream >> val;
		args = static_cast<T>(val);
	}


	template<typename T>
	struct SerialTag_Impl<T> {
		static_assert(!std::is_pointer<T>::value,
			"Serialised Array must provide size in following param");
		static void toStream(std::ostream &stream, T const &args)
		{
			globalToStream(stream, args);
		}

		static void fromStream(std::istream &stream, T &args)
		{
			globalFromStream(stream, args);
		}

	};

	template<typename T, typename size_type, typename... Rest>
	struct SerialTag_Impl<Serial_array_impl<T, size_type>, Rest...> {

		static void toStream(std::ostream &stream, Serial_array_impl<T, size_type> const &args, Rest const &... rest)
		{
			SerialTag_Impl<Serial_array_impl<T, size_type>>::toStream(stream, args);
			stream << SEP;
			SerialTag_Impl<Rest...>::toStream(stream, rest...);
		}
	
		static void fromStream(std::istream &stream, Serial_array_impl<T, size_type> &args, Rest &... rest)
		{
			SerialTag_Impl<Serial_array_impl<T, size_type>>::fromStream(stream, args);
			Check(DSERIALISE, stream.get() == SEP);
			SerialTag_Impl<Rest...>::fromStream(stream, rest...);
		}
	

	};

	template<typename T, typename size_type>
	struct SerialTag_Impl<Serial_array_impl<T, size_type>> {

		static_assert(std::is_integral<size_t>::value,
			"Serialised array must provide integral size in the following param");

		static void toStream(std::ostream &stream, Serial_array_impl<T, size_type> const &args)
		{
			stream << args.size;
			stream << SIZE_SEP<<LIST_START;
			for (size_type x = 0; x < args.size; ++x)
				stream << args.loc[x] << LIST_SEP;
			stream << LIST_END;
		}

		static void fromStream(std::istream &stream, Serial_array_impl<T, size_type> &args)
		{
			size_type count;
			stream >> count;
			Assert(DSERIALISE, count == args.size);
			Check(DSERIALISE, stream.get() == SIZE_SEP);
			Check(DSERIALISE, stream.get() == LIST_START);
			for (size_type x = 0; x < count; ++x)
			{
				stream >> args.loc[x];
				Check(DSERIALISE, stream.get() == LIST_SEP);
			}
			Check(DSERIALISE, stream.get() == LIST_END);
		}

	};


	template<typename T, typename... Rest>
	struct SerialTag_Impl<std::vector<T>, Rest...> {

		static void toStream(std::ostream &stream, std::vector<T> const &args, Rest const &... rest)
		{
			SerialTag_Impl<std::vector<T>>::toStream(stream, args);
			stream << SEP;
			SerialTag_Impl<Rest...>::toStream(stream, rest...);
		}
	
		static void fromStream(std::istream &stream, std::vector<T> &args, Rest &... rest)
		{
			SerialTag_Impl<std::vector<T>>::fromStream(stream, args);
			Check(DSERIALISE, stream.get() == SEP);
			SerialTag_Impl<Rest...>::fromStream(stream, rest...);
		}

	};

	template<typename T>
	struct SerialTag_Impl<std::vector<T>> {

		static void toStream(std::ostream &stream, std::vector<T> const &args)
		{
			stream << LIST_START;
			for (auto const &arg : args)
			{
				SerialTag_Impl<T>::toStream(stream, arg);
				stream << LIST_SEP;
			}
			stream << LIST_END;
		}

		static void fromStream(std::istream &stream, std::vector<T> &args)
		{
			Check(DSERIALISE, stream.get() == LIST_START);
			while (stream.peek() != LIST_END)
			{
				T next;
				SerialTag_Impl<T>::fromStream(stream, next);
				args.push_back(next);
				Check(DSERIALISE, stream.get() == LIST_SEP);
			}
			Check(DSERIALISE, stream.get() == LIST_END);
		}

	};
	template<typename T1, typename T2, typename... Rest>
	struct SerialTag_Impl<std::pair<T1,T2>, Rest...> {

		static void toStream(std::ostream &stream, std::pair<T1, T2> const &args, Rest const &... rest)
		{
			SerialTag_Impl<std::pair<T1, T2>>::toStream(stream, args);
			stream << SEP;
			SerialTag_Impl<Rest...>::toStream(stream, rest...);
		}

		static void fromStream(std::istream &stream, std::pair<T1, T2> &args, Rest &... rest)
		{
			SerialTag_Impl<std::pair<T1, T2>>::fromStream(stream, args);
			Check(DSERIALISE, stream.get() == SEP);
			SerialTag_Impl<Rest...>::fromStream(stream, rest...);
		}

	};

	template<typename T1, typename T2>
	struct SerialTag_Impl<std::pair<T1, T2>> {

		static void toStream(std::ostream &stream, std::pair<T1, T2> const &args)
		{
			stream << LIST_START;
			SerialTag_Impl<T1>::toStream(stream, args.first);
			stream << LIST_SEP;
			SerialTag_Impl<T2>::toStream(stream, args.second);
			stream << LIST_END;
		}

		static void fromStream(std::istream &stream, std::pair<T1,T2> &args)
		{
			Check(DSERIALISE, stream.get() == LIST_START);
			SerialTag_Impl<T1>::fromStream(stream, args.first);
			Check(DSERIALISE, stream.get() == LIST_SEP);
			SerialTag_Impl<T2>::fromStream(stream, args.second);
			Check(DSERIALISE, stream.get() == LIST_END);
		}

	};

	template< typename... Rest>
	struct SerialTag_Impl<std::string, Rest...> {

		static void toStream(std::ostream &stream, std::string const &args, Rest const &... rest)
		{
			SerialTag_Impl<std::string>::toStream(stream, args);
			stream << SEP;
			SerialTag_Impl<Rest...>::toStream(stream, rest...);
		}


		static void fromStream(std::istream &stream, std::string &args, Rest &... rest)
		{
			SerialTag_Impl<std::string>::fromStream(stream, args);
			Check(DSERIALISE, stream.get() == SEP);
			SerialTag_Impl<Rest...>::fromStream(stream, rest...);
		}

	};

	template<>
	struct SerialTag_Impl<std::string> {

		static void toStream(std::ostream &stream, std::string const &args)
		{
			int count = args.size();
			stream << count << SIZE_SEP << args;
		}

		static void fromStream(std::istream &stream, std::string &args)
		{
			int count;
			stream >> count;
			Check(DSERIALISE, stream.get() == SIZE_SEP);
			std::vector<char> charVector(count);
			stream.read(&charVector[0], count);
			Assert(DSERIALISE, !stream.fail());
			args = std::string(charVector.begin(), charVector.end());
		}

	};


	template <typename ...Types>
	SerialTag_Impl<Types...> GetSerialTag(Types... types)
	{
		return SerialTag<Types...>();
	}
};


/****************************/
/*   main Serialisable type */
/****************************/
class Serialisable
{
protected:
	friend std::ostream &operator << (std::ostream &outStream, Serialisable const &serialisable);
	friend std::istream &operator >> (std::istream &inStream, Serialisable &serialisable);

	virtual void PostRead() {};
	virtual void PreRead() {};

	void Write(std::ostream &stream) const
	{
		toStream(stream);
	}

	void Read(std::istream &stream)
	{
		PreRead();
		fromStream(stream);
		PostRead();
	}

	//virtual function bodies to be customised by each serialisable object
	virtual void toStream(std::ostream &stream) const = 0;
	virtual void fromStream(std::istream &stream) = 0;

	template<typename T>
	static T CreateFromStream_impl(std::istream &stream) {
		T var;
		stream >> var;
		return var;
	}


public:
	template<typename T>
	static T CreateFromStream(std::istream &stream) {
		class Child : private T {};
		static_assert(std::is_default_constructible< Child >::value,
			"Object created from stream must have default constructor(private is OK if friends with class Serial)");
		return CreateFromStream_impl<T>(stream);
	}
};

template <typename T, typename Size_Type >
SerialImplementation::Serial_array_impl<typename SerialImplementation::remove_ptr_const<T>::type, Size_Type> serial_const_array(T ptr, Size_Type size)
{
	return SerialImplementation::Serial_array_impl<typename SerialImplementation::remove_ptr_const<T>::type, Size_Type>(const_cast<SerialImplementation::remove_ptr_const<T>::type>(ptr), size);
}

inline
std::ostream &operator<<(std::ostream &outStream, Serialisable const &serialisable)
{
	serialisable.Write(outStream);
	return outStream;
}

inline
std::istream &operator>>(std::istream &inStream, Serialisable &serialisable)
{
	serialisable.Read(inStream);
	return inStream;
}

// ADD default implementation for tostream,fromstream and serial length via Serial_Tag
#define DECL_SERIAL(...)\
using SerialTag = decltype(SerialImplementation::GetSerialTag(__VA_ARGS__));\
void toStream(std::ostream &stream) const override { stream << SerialImplementation::OBJ_START;SerialTag::toStream(stream, __VA_ARGS__); stream << SerialImplementation::OBJ_END;}\
void fromStream(std::istream &stream) override { Check(DSERIALISE, stream.get() == SerialImplementation::OBJ_START);SerialTag::fromStream(stream,__VA_ARGS__);  Check(DSERIALISE,stream.get()==SerialImplementation::OBJ_END); }\
friend class Serialisable;


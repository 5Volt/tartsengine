#pragma once

#include <GMath.h>

/**
 * any additional data associated with an input event
 * i.e. modifier keys, mouse position, whether the action was a press or release, etc.
 **/
struct InputData {
	GMath::vec2 mousePosition;
	int input_action;

	//default constructor
	InputData()
		:mousePosition()
	{}
};

/**
 * input controller associated with a given input
 **/
enum Controller {
	INPUT_KEYBOARD,
	INPUT_MOUSE,
	INPUT_JOYSTICK, //not implemented
	NUM_INPUT_CONTROLLERS
};

/**
 * An input event recieved from GLFW
 **/
struct InputEvent {
	Controller inputController;
	int key;
	InputData data;
};
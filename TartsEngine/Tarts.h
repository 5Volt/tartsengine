#ifdef TARTS_EXPORTS
#define TARTS_API __declspec(dllexport)
#else
#define TARTS_API __declspec(dllimport)
#endif

#define _str(X) (std::string( #X ))
#ifdef _DEBUG
#define Check_(x) x;
#define Check_error(x) if (!(x)) throw std::exception((_str(check failed:) + _str(x)).c_str());
#define Check_warning(x) if (!(x)) std::cerr<<_str(check failed:) + _str(x);
#define Assert_(x)
#define Assert_error(x) if (!(x)) throw std::exception((_str(assertion failed:) + _str(x)).c_str());
#define Assert_warning(x) if (!(x)) std::cerr<<_str(assertion failed:) + _str(x);
#define cat(x,y) x##y
#define Check(checkID, x) cat(Check_,checkID)(x);
#define Assert(checkID, x) cat(Assert_,checkID)(x);
#else
#define Check(checkID, x) x;
#define Assert(checkID, x)
#endif // DEBUG

#define DSERIALISE error
#define DASSET	   error

#include <GL\glew.h>
#include "StaticMesh.h"

StaticMesh::StaticMesh()
{
}

StaticMesh::StaticMesh(VertexType const &_vertexType)
	: StaticMesh(_vertexType,nullptr,0)
{}

StaticMesh::StaticMesh(VertexType const &_vertexType, std::vector<float> const &_data, std::vector<VertexGroup> _vertexGroups)
	: StaticMesh(_vertexType, &data[0],data.size()/_vertexType.width(), _vertexGroups)
{}


StaticMesh::StaticMesh(VertexType const &_vertexType, float *_data, unsigned int numVertices, std::vector<VertexGroup> _vertexGroups)
	: currentState(MeshState::NEW)
	, vertexType(_vertexType)
	, data(_data, _data + numVertices * vertexType.width())
	, vertexGroups(_vertexGroups)
{}

StaticMesh::~StaticMesh()
{
}

bool StaticMesh::Initialise()
{
	if (vertexBuffer != -1)
		return true;

	glGenBuffers(1, &vertex_buffer);
	if(vertex_buffer != -1)
		currentState = MeshState::INITIALISED;

	return vertex_buffer != -1;
}

bool StaticMesh::Load()
{
	if (!Initialise())
		return false;
	if (currentState == MeshState::LOADED)
		return true;

	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data[0]) * data.size(), &data[0], GL_STATIC_DRAW);

	numVertices = data.size() / vertexType.width();

	currentState = MeshState::LOADED;
}

size_t StaticMesh::VertexWidth() const
{
	return vertexType.width();
}

bool StaticMesh::SetInShader(Shader const & shader) const
{
	//offset of the current attribute in the data array
	int offsetSum = 0;
	for (auto const &x : vertexType.attributes)
	{
		GLuint attrib_location = shader.GetVariable(x.first).location;
		glEnableVertexAttribArray(attrib_location);
		glVertexAttribPointer(attrib_location, x.second, GL_FLOAT, GL_FALSE,
			sizeof(float) * VertexWidth(), (float*)(sizeof(float) * offsetSum));
		offsetSum += x.second;
	}

	return true;
}

bool StaticMesh::DrawWithShader(Shader const & shader) const
{
	if (!SetInShader(shader))
		return false;

	//draw each vertex group in turn. 
	//Vertices which are not included in a vertex group cannot be drawn
	//as we do not know what primative type to draw them with.
	for (auto const & vGroup : vertexGroups)
	{
		glDrawArrays( vGroup.drawType, vGroup.first, vGroup.count );
	}
	
	return true;
}

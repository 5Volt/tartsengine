#pragma once

#include <vector>
#include <queue>

#include "Interfaces/Serialisable.h"
#include "Shader.h"

struct VertexType : public Serialisable
{
	using AttrWidth = std::pair<ShaderAttribute, size_t>;

	std::vector<AttrWidth> attributes;

	unsigned int width() const
	{
		unsigned int sum = 0;
		for (auto x : attributes)
			sum += x.second;
		return sum;
	}

	DECL_SERIAL(attributes);

};


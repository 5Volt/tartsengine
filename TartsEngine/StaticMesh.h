#pragma once

#include "Tarts.h"

#include <cstdint>
#include <vector>

#include <GL/glew.h>

#include "Util/Asset.h"

#include "VertexType.h"

/***
 ** A vertex group is a mapping between bits of vertex data in a static mesh
 ** and the OpenGL Primative draw mode which should be used to render them
 ** All vertices in a mesh need to be part of a vertex group in order to be drawn
 ** In Skeletal Mesh objects, the vertex groups are mapped many to one with 
 ** bone transformations. 
 ** i.e. multiple vertex groups can be affected by a single bone.
 **/
struct VertexGroup : Serialisable
{
	GLint drawType;
	size_t first, count;

	DECL_SERIAL(drawType, first, count);
};

/***
 ** A Static mesh contains vertices in the form of coordinate data 
 ** as well as any additional properties required to draw the vertex
 ** including vertex color, UV maps, or anything else that can be represented 
 ** in a known ShaderAttribute.
 **/
class StaticMesh : public Asset<StaticMesh>
{
public:
	TARTS_API
		StaticMesh();
	TARTS_API
		StaticMesh(VertexType const &vertexType);
	TARTS_API
		StaticMesh(VertexType const &vertexType, std::vector<float> const &data, std::vector<VertexGroup> _vertexGroups = std::vector<VertexGroup>());
	TARTS_API
		StaticMesh(VertexType const &vertexType, float *data, unsigned int numVertices, std::vector<VertexGroup> _vertexGroups = std::vector<VertexGroup>());

	TARTS_API
		~StaticMesh();

	//possible states over the lifetime of a mesh
	enum class MeshState : uint8_t
	{
		//just created
		NEW = 0,
		//buffers and IDS allocated in OpenGL
		INITIALISED = 1,
		//mesh data loaded to GPU
		LOADED = 2,
		//mesh data loaded to GPU is out of date
		DIRTY = 3
	};
	
	//initialise buffers on the GPU
	TARTS_API
	bool Initialise();

	//Load mesh data to the GPU
	TARTS_API
	bool Load();
	
	TARTS_API
		size_t VertexWidth() const;

	TARTS_API
		bool SetInShader(Shader const &shader) const;
	
	TARTS_API
		bool DrawWithShader(Shader const &shader) const;

protected:
	//run-time state data
	MeshState currentState;
	GLuint vertexBuffer;

	VertexType vertexType;
	std::vector<float> data;

	std::vector<VertexGroup> vertexGroups;

	GLuint vertex_buffer;
	size_t numVertices;

	DECL_SERIAL(vertexType, data, vertexGroups)
	
};


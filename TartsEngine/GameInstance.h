#pragma once

#include "Tarts.h"

#include <GLFW/glfw3.h>

#include "GraphicsContext.h"
#include "GameController.h"
#include "Interfaces\InputHandler.h"
#include "InputListener.h"

/**
 * Basic instance of a game, this class is the full instance of an engine and will take over the thread when run
 * This class owns all in game entities (descending from GameObject) and will manage asset instantiation as well 
 * as well as ticking for all game objects(frameTick) and physics objects(physicsTick)
 * it will also pass down HID events to a GameController type, which will be instantiated and owned by the GameInstance
 * while a game is running, it maintians exclusive ownership of at least one GLFW based graphics context
 **/
template<typename StateType = GameState, typename ControllerType = GameController<GameState>, typename GCType = GraphicsContext>
class GameInstance 
{
	static_assert(std::is_base_of<GameState, StateType>::value,
		"GameInstance::StateType template param must be base class of GameState");
	static_assert(std::is_base_of<GameController<StateType>, ControllerType>::value,
		"GameInstance::ControllerType template param must be base class of GameController");
	static_assert(std::is_base_of<GraphicsContext, GCType>::value, 
		"GameInstance::GCType template param must be base class of GraphicsContext");

public:
	GameInstance(): state(new StateType()), main_gc(new GCType()), main_controller(new ControllerType(state))
	{ InputListener::StartListening(main_gc->GetWindow(), main_controller); };
	~GameInstance() {};

	void Run()
	{
		if (!Initialise())
			std::exception("Game Instance failed to initialise");

		while (!state->stopped && !glfwWindowShouldClose(main_gc->GetWindow())) {
			previous_update_time = this_update_time;
			this_update_time = glfwGetTime();
			auto deltaT = this_update_time - previous_update_time;

			FrameTick(deltaT);
		}

	}

	GCType const &GetMainContext() { return main_gc; }
	ControllerType const &GetMainController() { return main_controller; }

protected:

	virtual bool Initialise() { return true; }
	virtual void FrameTick(double deltaT) { main_gc->SwapBuffers(); }

protected:
	std::shared_ptr<StateType> state;
	std::shared_ptr<GCType> main_gc;
	std::shared_ptr<ControllerType>  main_controller;

	double previous_update_time, this_update_time;
};


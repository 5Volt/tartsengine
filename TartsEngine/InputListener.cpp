#include "InputListener.h"

#include "Interfaces/InputHandler.h"

#include "GL/glew.h"
#include<GLFW/glfw3.h>

using namespace std;
using namespace GMath;

map <GLFWwindow *, std::weak_ptr<InputHandler> > InputListener::windowHandlers;
map <GLFWwindow *, GMath::vec2 > InputListener::mousePosition;


shared_ptr<InputHandler> InputListener::get_handler(GLFWwindow * window)
{
	if (windowHandlers.find(window) == windowHandlers.end())
		return nullptr;

	auto current_handler = windowHandlers.at(window).lock();
	if (!current_handler)
		throw exception("input recieved from window with a dead handler");
	return current_handler;
}

/** @brief (one liner)
*
* (documentation goes here)
*/
void InputListener::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	auto current_handler = get_handler(window);
	//construct an InputEvent
	InputEvent evt;
	evt.inputController = INPUT_KEYBOARD;
	evt.key = key;
	evt.data.input_action = action;
	evt.data.mousePosition = mousePosition[window];
	
	current_handler->HandleInputEvent(evt);
}

/** @brief (one liner)
*
* (documentation goes here)
*/
void InputListener::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	auto current_handler = get_handler(window);

	InputEvent evt;
	evt.inputController = INPUT_MOUSE;
	evt.key = button;
	evt.data.input_action = action;
	evt.data.mousePosition = mousePosition[window];

	current_handler->HandleInputEvent(evt);
}

/** @brief (one liner)
*
* (documentation goes here)
*/
void InputListener::mouse_position_callback(GLFWwindow* window, double xPos, double yPos)
{
	mousePosition[window] = vec<2, double>(xPos, yPos);
}

/** @brief (one liner)
*
* (documentation goes here)
*/
void InputListener::StartListening(GLFWwindow *window, std::shared_ptr<InputHandler> handler)
{
	if (windowHandlers.find(window) != windowHandlers.end())
		throw exception("this window already has an active handler");

	windowHandlers[window] = handler;

	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, mouse_position_callback);
}


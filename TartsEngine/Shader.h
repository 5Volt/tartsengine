#pragma once

#include<memory>
#include<string>
#include<vector>

#include "Tarts.h"

#include <GLFW\glfw3.h>

#include "Interfaces/Serialisable.h"
#include "Util/Variant.h"
#include "Object.h"
#include "Util/Asset.h"

enum class ShaderUniform : uint8_t
{
	MVP,
	LIGHTPOS,
	LIGHTCOL,
	LIGHTSTR,
	TEXTUREID1,
	TEXTUREID2,
	CUSTOM
};

enum class ShaderAttribute : uint8_t
{
	VERTPOS,
	VERTCOL,
	VERTUV,
	VERTNORM,
	CUSTOM
};

struct ShaderVariable : public Serialisable
{
	using VariableID = Variant<ShaderUniform, ShaderAttribute>;
	
	std::string name;
	VariableID ID;
	
	GLuint location = -1;
	
	TARTS_API
	bool isUniform()
	{
		return ID.active == VariableID::GetId<ShaderUniform>::ID;
	}

	TARTS_API
	bool isAttribute()
	{
		return ID.active == VariableID::GetId<ShaderAttribute>::ID;
	}

	TARTS_API
	ShaderVariable() = default;

	TARTS_API
	ShaderVariable(ShaderUniform const & _ID, std::string const &_name) {
		name = _name;
		ID = _ID;
	}

	TARTS_API
	ShaderVariable(ShaderAttribute const & _ID, std::string const &_name) {
		name = _name;
		ID = _ID;
	}

	DECL_SERIAL(name, ID);

};

struct ShaderSource : public Serialisable
{
	std::string vertexSource;
	std::string fragmentSource;

	std::vector<ShaderVariable> variables;

	DECL_SERIAL(vertexSource, fragmentSource, variables);
};

/**
 * A GLSL shader.
 * This class handles loading and compiling shaders via the opengl driver
 * this class maintains the compiled shader entities handle in openGL
 * a given openGL shader is only valid while its associated shader class exists
 **/
class Shader : public Asset<Shader>
{
public:
	TARTS_API
	Shader(ShaderSource &&source);
	
	TARTS_API
	~Shader() = default;

	TARTS_API
	GLuint GetHandle() const { return shaderHandle; }

	TARTS_API
	ShaderVariable const &GetVariable(ShaderVariable::VariableID id) const;
	
	TARTS_API
	ShaderVariable const &GetVariable(std::string const &varName) const;

	Shader() = default; // default constructor to allow creating from streams
private:
	void compile();

	void FindVariables();

	ShaderSource source;
	GLuint shaderHandle;

	DECL_SERIAL(source);

	
	TARTS_API
	void PostRead() override;
	
};

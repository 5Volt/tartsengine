
#include "Util/Asset.h"

#include "Shader.h"
#include "StaticMesh.h"

using namespace std;

using EngineAssets = TypeList<
	StaticMesh,Shader
>; 

void RegisterAllEngineAssets()
{
	AssetFactory::RegisterAll<EngineAssets>();
}
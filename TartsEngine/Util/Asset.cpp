
#include "Asset.h"

#include <fstream>

using namespace std;

std::map<AssetID, AssetFactory::CreatorFunction> AssetFactory::creators;

void AssetFactory::Register(AssetID const &id, AssetFactory::CreatorFunction func)
{
	creators[id] = func;
}

bool AssetFactory::HasID(AssetID const &id)
{
	return creators.find(id) != creators.end();
}

AssetFactory::FactoryObjRef AssetFactory::Create(AssetID id)
{
	auto creatorIt = creators.find(id);
	if (creatorIt != creators.end())
	{
		return creatorIt->second();
	}

	return nullptr;
}


AssetPackage::Path AssetPackage::GetAssetFilePath(Path const &assetName) const
{
	Path assetFileName = rootDir;
	assetFileName += assetName;
	assetFileName += ASSET_FILE_EXTENSION;
	return assetFileName;
}

void AssetPackage::WriteAsset(AssetFactory::FactoryObjRef const asset, Path const &assetName) const
{
	Path assetFileName = GetAssetFilePath(assetName);

	std::fstream file(assetFileName, fstream::out);

	AssetHeader header;
	header.id = asset->GetDynamicId();
	file << header << *asset;
}

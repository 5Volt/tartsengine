#pragma once


template<typename... Types>
struct TypeList;

template<typename first, typename... rest>
struct TypeList<first, rest...>
{
	using Head = first;
	using Tail = TypeList<rest...>;

	template<template<class> class LambdaType, typename ...Args>
	static bool ForEach(Args ...args)
	{
		bool A = LambdaType<Head>::Eval(args...);
		bool B = Tail::ForEach<LambdaType>(args...);
		return A || B;
	}


	template<template<class> class LambdaType, typename ...Args>
	static bool ForAll(Args ...args)
	{
		return LambdaType<Head>::Eval(args...) && Tail::ForAll<LambdaType>(args...);
	}

	template<template<class> class LambdaType, typename ...Args>
	static bool ForAny(Args ...args)
	{
		return LambdaType<Head>::Eval(args...) || Tail::ForAll<LambdaType>(args...);
	}

};

template<typename first>
struct TypeList<first>
{
	using Head = first;
	using Tail = void;

	template<template<class> class LambdaType,typename ...Args>
	static bool ForEach(Args ...args)
	{
		return LambdaType<Head>::Eval(args...);
	}

	template<template<class> class LambdaType, typename ...Args>
	static bool ForAll(Args ...args)
	{
		return LambdaType<Head>::Eval(args...);
	}


	template<template<class> class LambdaType, typename ...Args>
	static bool ForAny(Args ...args)
	{
		return LambdaType<Head>::Eval(args...);
	}


};

template<typename Type>
struct is_typelist : std::bool_constant<false> {};

template<typename... Types>
struct is_typelist<TypeList<Types...>> :std::bool_constant<true> {};
#pragma once

#include <algorithm>
#include <exception>

#include "../Tarts.h"

#include "../Interfaces/Serialisable.h"

namespace VariantImplementation
{

	template <typename ...Args>
	struct Variant_impl;

	template <typename Type, typename VariantImpl>
	struct VariantTypeId;

	using VariantID = size_t;
	static constexpr VariantID VAR_ID_NULL = 0;
	using Variant_null = Variant_impl<>;

	template <typename T, typename... Args>
	struct Variant_impl<T, Args...>
	{
		using Type = T;
		using Tail = Variant_impl<Args...>;
		static constexpr size_t size = std::max(sizeof(Type), Tail::size);
		static constexpr VariantID ID = Tail::ID + 1;

		static_assert(!VariantTypeId<T, Tail>::value,
			"Variant types must be unique within a single variant");
		
		static size_t SizeByID(VariantID const &id) {
			return id == ID ? sizeof(Type) : Tail::SizeByID(id);
		}

		template<typename CompType>
		static bool EquivToActive(VariantID const &activeId, char const *data, CompType const &test) {
			return activeId == ID ? test == *reinterpret_cast<Type const*>(data) : Tail::EquivToActive(activeId, data, test);
		}



	};

	template<typename T>
	struct Variant_impl<T>
	{
		using Type = T;
		using Tail = Variant_null;
		static constexpr size_t size = sizeof(Type);
		static constexpr VariantID ID = VAR_ID_NULL + 1;

		static size_t SizeByID(VariantID const &id) {
			return id == ID ? sizeof(Type) : 0;
		}

		template<typename CompType>
		static bool EquivToActive(VariantID const &activeId, char const *data, CompType const &test) {
			return activeId == ID ? test == *reinterpret_cast<Type const*>(data) : false;
		}

	};

	template<>
	struct Variant_impl<>
	{
		using Type = void;
		using Tail = Variant_null;

		static constexpr size_t size = 0;
		static constexpr VariantID ID = VAR_ID_NULL;
	};


	template<typename Type, typename VariantImpl>
	struct VariantTypeId
	{
		static constexpr bool value = std::is_same<Type, VariantImpl::Type>::value || VariantTypeId<Type, VariantImpl::Tail>::value;
		static constexpr VariantID ID = std::is_same<Type, VariantImpl::Type>::value * VariantImpl::ID + VariantTypeId<Type, VariantImpl::Tail>::ID;
	};

	template<typename Type>
	struct VariantTypeId <Type, Variant_impl<Type>>
	{
		static constexpr bool value = std::is_same<Type, Variant_impl<Type>::Type>::value;
		static constexpr VariantID ID = std::is_same<Type, Variant_impl<Type>::Type>::value *  Variant_impl<Type>::ID;
	};

	template<typename Type>
	struct VariantTypeId <Type, Variant_null>
	{
		static constexpr bool value = false;
		static constexpr VariantID ID = VAR_ID_NULL;
	};
}

template<typename... Args>
struct Variant : public Serialisable
{
	using Implementation = VariantImplementation::Variant_impl<Args...>;
	template<typename T>
	using GetId = VariantImplementation::VariantTypeId<T, Implementation>;
	using VariantID = VariantImplementation::VariantID;

	char data[Implementation::size];

	VariantID active = VariantImplementation::VAR_ID_NULL;

	Variant() = default;

	template <typename AsType,
		std::enable_if_t<GetId<AsType>::value, int> = 0>
	Variant(AsType const &val)
	{
		*this = val;
	}

	template<typename AsType>
	AsType const &get() const
	{
		static_assert(GetId<AsType>::value,
			"cannot Variant does not contain type, retrival cannot be resolved");

		if (active != GetId<AsType>::ID)
			throw std::exception("variant get as type which is not currently active");

		return *reinterpret_cast<AsType const*>(data);
	}
	
	template<typename AsType>
	void set(AsType const &val)
	{
		static_assert(GetId<AsType>::value,
			"cannot Variant does not contain type, assignment cannot be resolved");

		*reinterpret_cast<AsType*>(data) = val;
		active = GetId<AsType>::ID;
	}

	template<typename AsType>
	Variant<Args...> &operator=(AsType const &val)
	{
		set(val);
		return *this;
	}

	template<>
	Variant<Args...> &operator=(Variant<Args...> const &other) = default;

	template<typename AsType>
	bool operator==(AsType const &val) const
	{
		static_assert(GetId<AsType>::value || !std::is_class<AsType>::value,
			"Cannot compare variant to any type which is not a built in or contained in the variant");
		
		if constexpr (std::is_class<AsType>::value)
		{
			if (active != GetId<AsType>)
				return false;
		}
		else
		{
			return Implementation::EquivToActive(active, data, val);
		}
	}

	template<>
	bool operator==<Variant<Args...>>(Variant<Args...> const &other) const
	{
		if (other.active != active)
			return false;

		auto checkBytes = Implementation::SizeByID(active);
		for (decltype(checkBytes) x = 0; x < checkBytes; ++x)
		{
			if (data[x] != other.data[x])
				return false;
		}
		return true;
	}
	

	template<typename AsType>
	bool operator!=(AsType const &val) const
	{
		return !(*this == other);
	}

	template<>
	bool operator!=<Variant<Args...>>(Variant<Args...> const &other) const
	{
		return !(*this == other);
	}

private:

	DECL_SERIAL(active, serial_const_array(data, Implementation::size));

};
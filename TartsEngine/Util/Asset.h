#pragma once

#include <filesystem>
#include <fstream>
#include <functional>
#include <map>
#include <memory>
#include <string>

#include "TypeList.h"
#include "../Interfaces/Serialisable.h"
#include "../Object.h"

const std::string ASSET_FILE_EXTENSION = ".tea";

using AssetID = std::string;

struct AssetHeader : public Serialisable
{
	AssetID id;
	DECL_SERIAL(id);
};

struct Asset_Base : public virtual Object, public virtual Serialisable
{
	virtual AssetID const &GetDynamicId() = 0;
};


/***
** An Asset Factory is a runtime map between unique string identifiers and creation methods for class types
** the factory is a static object contained within a single engine instance
** At engine initialisation time, all objects which can be Created by the asset factory must be registered.
** when implementing a concrete game, a typelist should be created containing all the asset types of the 
** game and registered during game initialisation time
***/
struct AssetFactory
{
	using FactoryBaseType = Asset_Base;
	template<typename T>
	using FactoryPtrType = std::shared_ptr<T>;
	using FactoryObjRef = FactoryPtrType<FactoryBaseType>;
	using CreatorFunction = std::function<FactoryObjRef()>;
	static std::map<AssetID, CreatorFunction> creators;

	TARTS_API
		static void Register(AssetID const &id, CreatorFunction func);

	TARTS_API
		static bool HasID(AssetID const &id);
	
	template<typename T>
	static bool HasType()
	{
		return HasID(Asset<T>::id);
	}

	//only enabled if passing one type and it's a typelist
	template<typename TypeListType>
	static void RegisterAll()
	{
		static_assert(is_typelist<TypeListType>::value,
			"RegisterAll() template param must be a typelist");
		TypeListType::ForEach<RegisterClass>();
	}

	TARTS_API
		static FactoryObjRef Create(AssetID id);
	private:
		
		template<typename T>
		struct RegisterClass
		{
			static bool Eval() { Asset<T>::Register(); return true; }
		};

};

/***
** An Asset package is a collection of asset instances stored in the filesystem.
** The package object iteslf knows nothing about the possible types of assets therin
** A factory type must be supplied to handle the creation and destruction of objects in the package
***/
struct AssetPackage
{
	using Path = std::filesystem::path;

	const Path rootDir;

	template <typename T>
	using FactoryPtrType = typename AssetFactory::FactoryPtrType<T>;

	AssetPackage(Path const &root)
		:rootDir(root)
	{}

	template<typename T>
	FactoryPtrType<T> GetAsset(Path const &assetName) const
	{
		Path assetFileName = GetAssetFilePath(assetName);
		
		auto parentFolder = assetFileName.parent_path();
		std::filesystem::create_directory(parentFolder);
		
		std::fstream file(assetFileName);

		Assert(DASSET,file.is_open());
		
		AssetHeader header;
		file >> header;

		auto obj = std::dynamic_pointer_cast<T>(AssetFactory::Create(header.id));

		file >> *obj;
		return obj;
	}

	TARTS_API
	Path GetAssetFilePath(Path const &assetName) const;

	TARTS_API
	void WriteAsset(AssetFactory::FactoryObjRef const asset, Path const &assetPath) const;

};

/***
** Test to determine whether a given class has the functionality required to be an asset
** assets must be default constructible and a child of the 'Serialisable' and 'Object' types
***/
template<typename T>
struct is_assetisable : std::bool_constant<std::is_default_constructible<T>::value 
	&& std::is_base_of<Serialisable,T>::value 
	&& std::is_base_of<AssetFactory::FactoryBaseType, T>::value>
{};

template<typename T> 
struct Asset : public virtual Asset_Base
{
	//static_assert(is_assetisable<T>::value,
	//	"class type is not an asset");

	static AssetID const id;
	using FactoryObjRef = AssetFactory::FactoryObjRef;

	virtual AssetID const &GetDynamicId()
	{
		return id;
	}

	static AssetID const &GetStaticId()
	{
		return id;
	}


	static FactoryObjRef CreateFunc()
	{
		return std::static_pointer_cast<FactoryObjRef::element_type>(std::make_shared<T>());
	}

	static bool Register()
	{
		AssetFactory::Register(id, &CreateFunc);
		return true;
	}

};

template<typename T>
AssetID const Asset<T>::id = typeid(T).name();


#ifndef GMATH3D_H_INCLUDED
#define GMATH3D_H_INCLUDED
//TODO document
/**
 * This file declares and defines some common and useful functions to operator on
 * 3d standard or homogenous coordinates i.e. predominantly vec3 and mat4.
 * Ideally this file will be included instead of the GMath-Core library when required.
**/

/**NB regarding extensions:
 * the extended libraries will not use specific namespacing either using the c++ namespace
 * or manually(e.g. Rotate3d(mat4) instead of Rotate(mat4)).
**/

//include the core library
#include"GMath.h"
namespace GMath {
//vector properties
template<typename T>
T azimuth(const vec<3,T> &v) {
    return atan2(v.x(),v.z());
}

template<typename T>
T elevation(const vec<3,T> &v) {
    return sin(v.y()/v.length());
}

//TODO rotation is only occuring around the z axis 
template<typename fromT, typename T>
void rotate(mat<4, fromT> &matrix, T rotation) {
	mat<4, T> transform;
	transform[0][0] = transform[1][1] = cos(rotation);
	transform[1][0] = -(transform[0][1] = sin(rotation));
	matrix *= transform;
}

template<typename fromT, typename fromT2, typename T>
void rotate(mat<4, fromT> &matrix, vec<3, fromT2> const & axis, T radians)
{
	vec3 uAxis = normalise(axis);
	auto l = uAxis.x(),
		m = uAxis.y(),
		n = uAxis.z();
	auto sinT = sin(radians);
	auto cosT = cos(radians);
	auto oneMinusCos = 1.0 - cosT;
	auto ll = l * l;
	auto lm = l * m;
	auto ln = l * n;
	auto mm = m * m;
	auto mn = n * m;
	auto nn = n * n;
	float matData[] = 
		  { ll*oneMinusCos + cosT, lm*oneMinusCos - n * sinT, ln*oneMinusCos + m * sinT, 0.0,
			lm*oneMinusCos + n * sinT, mm*oneMinusCos + cosT, mn*oneMinusCos - l * sinT, 0.0,
			ln*oneMinusCos - m * sinT, mn * oneMinusCos + l * sinT, nn*oneMinusCos + cosT, 0,
			0.0, 0.0, 0.0, 1.0 };
	mat4 rotationMatrix(matData);

	matrix *= rotationMatrix;
}

template<typename fromT, typename T>
void translate(mat<4, fromT> &matrix, vec<3, T> const &translation)
{
	mat<4, fromT> transform;
	transform[0][2] = translation.x();
	transform[1][2] = translation.y();
	transform[2][2] = translation.z();

	matrix *= transform;
}

}
#endif // GMATH3D_H_INCLUDED

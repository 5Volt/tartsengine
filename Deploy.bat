

@echo off

xcopy "%1*.h" "%3\include\TartsEngine\" /S /Y
xcopy "%2TartsEngine.dll" "%3\lib\"  /Y
xcopy "%2TartsEngine.lib" "%3\lib\"  /Y
xcopy "%2fmodL.dll" "%3\lib\"  /Y
xcopy "%2glew32d.dll" "%3\lib\"  /Y
xcopy "%2glfw3.dll" "%3\lib\"  /Y

exit /B 0
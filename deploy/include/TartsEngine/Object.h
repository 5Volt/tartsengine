#pragma once

#include "Tarts.h"

using ObjectID = unsigned long;

/**
 * An object managed by the TartsEngine memory manager
 * Any object which you wish to be owned by the engine must extend from this class
 **/
class Object
{
public:
	TARTS_API
	Object() = default;
	
	TARTS_API
	virtual ~Object() = default;

};


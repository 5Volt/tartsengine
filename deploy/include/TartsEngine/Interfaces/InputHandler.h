#pragma once

/**
 * Class to handle HID Input events
 **/
class InputHandler
{
public:
	/**
	 * @brief Handle a single input event (key press, mouse click)
	 * @param InputEvent the event which was just recieved
	 **/
	virtual bool HandleInputEvent(struct InputEvent const &evt) = 0;
};

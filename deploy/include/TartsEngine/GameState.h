#pragma once

#include "Tarts.h"

/**
 * State object for a basic game
 * This class should be extended in your project to encapsulate
 * all elements of the current state of your game
 * such as:
 * characters which are currently acting in your game
 * any timers or scores
 * the level which is currently loaded
 * sound track information
 * etc
 **/
struct GameState
{
public:
	TARTS_API
	GameState();
	TARTS_API
	~GameState();

	bool stopped = false;
};


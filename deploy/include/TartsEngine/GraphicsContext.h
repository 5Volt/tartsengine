#pragma once

#include <memory>

#include "Tarts.h"

struct GLFWwindow;

/**
 * information surrounding an active OpenGL graphics context
 * including the window to which it is bound, loaded shaders,etc
 **/
class GraphicsContext
{
public:
	TARTS_API
	GraphicsContext();

	TARTS_API
	~GraphicsContext();
	
	/**
	 * swap draw buffer to screen and process any input events
	 * should be called once per frame
	 **/
	TARTS_API
		void SwapBuffers();

	TARTS_API
		GLFWwindow *GetWindow() const
	{
		return main_window;
	}
private:
	// this window must be owned as a raw ptr
	GLFWwindow* main_window;

};

